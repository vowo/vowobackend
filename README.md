# vowo.io Gateway

Microservice providing the gateway functionality of vowo.io

[![pipeline status](https://gitlab.com/vowo/vowobackend/badges/master/pipeline.svg)](https://gitlab.com/vowo/vowobackend/commits/master)

## Run it

```bash
dep ensure
docker run -d -p6831:6831/udp -p16686:16686 jaegertracing/all-in-one:latest
go build -v -o ./output/vowo . && ./output/vowo gateway
```

## If port is locked

```bash
lsof -nP -i4TCP:8081 | grep LISTEN
```
