package cmd

import (
	"net"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"gitlab.com/vowo/vowobackend/services/gateway"
	"go.uber.org/zap"
)

// gatewayCommand represents the gateway command
var gatewayCommand = &cobra.Command{
	Use:   "gateway",
	Short: "Starts Gateway service",
	Long:  `Starts Gateway service.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		zapLogger := logger.With(zap.String("service", "gateway"))
		logger := log.NewFactory(zapLogger)
		server := gateway.NewServer(
			net.JoinHostPort(gatewayOptions.serverInterface, strconv.Itoa(gatewayOptions.serverPort)),
			net.JoinHostPort(gatewayOptions.serverInterface, strconv.Itoa(gatewayOptions.serverHealthPort)),
			tracing.Init("gateway", metricsFactory.Namespace("gateway", nil), logger, jAgentHostPort),
			metricsFactory,
			logger,
			jAgentHostPort,
		)

		go server.RunHealthAndReadinessProbes()
		return logError(zapLogger, server.Run())
	},
}

var (
	gatewayOptions struct {
		serverInterface  string
		serverPort       int
		serverHealthPort int
	}
)

func init() {
	RootCmd.AddCommand(gatewayCommand)

	gatewayCommand.Flags().StringVarP(&gatewayOptions.serverInterface, "bind", "", "0.0.0.0", "interface to which the Gateway server will bind")
	gatewayCommand.Flags().IntVarP(&gatewayOptions.serverPort, "port", "p", 8081, "port on which the Gateway server will listen")
	gatewayCommand.Flags().IntVarP(&gatewayOptions.serverHealthPort, "healthPort", "e", 8181, "port on which the health check of the Gateway server will listen")
}
