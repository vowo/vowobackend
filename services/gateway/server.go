package gateway

import (
	"net/http"

	"github.com/emanoelxavier/openid2go/openid"
	"github.com/gorilla/context"
	"github.com/gorilla/handlers"
	"github.com/satori/go.uuid"

	"gitlab.com/vowo/vowo-corelib/pkg/httputils"

	"github.com/gorilla/mux"
	"github.com/heptiolabs/healthcheck"
	_ "github.com/joho/godotenv/autoload" //automatically load env variables
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-lib/metrics"
	"gitlab.com/vowo/vowo-corelib/pkg/httperr"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"go.uber.org/zap"
)

// Server implements Gateway service
type Server struct {
	hostPort       string
	healthHostPort string
	tracer         opentracing.Tracer
	logger         log.Factory
	products       *productsHandler
}

// NewServer creates a new Gateway server
func NewServer(hostPort string, healthHostPort string, tracer opentracing.Tracer, metricsFactory metrics.Factory, logger log.Factory, jAgentHostPort string) *Server {
	return &Server{
		hostPort:       hostPort,
		healthHostPort: healthHostPort,
		tracer:         tracer,
		logger:         logger,
		products:       newProductsHandler(tracer, logger),
	}
}

// Run starts the Gateway server
func (s *Server) Run() error {
	mux := s.createServeMux()

	s.logger.Bg().Info("Starting", zap.String("address", "http://"+s.hostPort))
	return http.ListenAndServe(s.hostPort, mux)
}

// RunHealthAndReadinessProbes starts health and readiness probe for gateway
func (s *Server) RunHealthAndReadinessProbes() {
	health := healthcheck.NewHandler()

	//TODO implement proper checks
	// Our app is not happy if we've got more than 100 goroutines running.
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))
	s.logger.Bg().Info("Starting Healthcheck", zap.String("address", "http://"+s.healthHostPort))
	err := http.ListenAndServe(s.healthHostPort, health)
	if err != nil {
		s.logger.Bg().Error("could not start readiness probe", zap.Error(err))
	}
}

func (s *Server) createServeMux() http.Handler {
	mux := tracing.NewServeMux(s.tracer)
	mux.Handle("/", s.makeHandler())
	return mux
}

func (s *Server) makeHandler() http.Handler {
	r := mux.NewRouter()

	//setup routes
	r.Handle("/products", http.HandlerFunc(s.getAllProducts)).Methods("GET")
	r.Handle("/products/{productID}", http.HandlerFunc(s.getProductByID)).Methods("GET")
	r.Handle("/traces/{traceID}", http.HandlerFunc(s.getTraceByID)).Methods("GET")
	r.NotFoundHandler = http.HandlerFunc(httperr.HandleNotFound)
	r.MethodNotAllowedHandler = http.HandlerFunc(httperr.HandleMethodNotAllowed)

	//setup auth
	providers, err := getProviders()
	if err != nil {
		s.logger.Bg().Fatal("could not setup auth providers", zap.Error(err))
	}
	authorizedRouter := r.PathPrefix("/me").Subrouter()
	authorizedRouter.Handle("/info", http.HandlerFunc(s.getMyInfo)).Methods("GET")
	authorizedRouter.Use(AuthMiddleware(providers, s.logger))

	//setup cors
	methodsOk := handlers.AllowedMethods([]string{http.MethodGet, http.MethodHead, http.MethodPost, http.MethodPut, http.MethodOptions})
	credentialsOk := handlers.AllowCredentials()
	originsOk := handlers.AllowedOrigins([]string{"*"})
	headersOk := handlers.AllowedHeaders([]string{"Content-Type", "Authorization", "X-Requested-With", "Accept"})
	handler := handlers.CORS(methodsOk, headersOk, credentialsOk, originsOk)(r)

	return handler
}

func (s *Server) getAllProducts(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	response, err := s.products.GetAllProducts(ctx)
	if httperr.HandleError(w, err, http.StatusInternalServerError) {
		s.logger.For(ctx).Error("request failed", zap.Error(err))
		return
	}

	responseErr := httputils.EncodeHTTPGenericResponse(ctx, w, response)
	if httperr.HandleError(w, responseErr, http.StatusInternalServerError) {
		s.logger.For(ctx).Error("cannot marshal response", zap.Error(err))
	}
}

func (s *Server) getProductByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	productID := mux.Vars(r)["productID"]
	productUUID, err := uuid.FromString(productID)
	if httperr.HandleError(w, err, http.StatusBadRequest) {
		s.logger.For(ctx).Info("could not parse uuid", zap.Error(err))
		return
	}

	response, err := s.products.GetProductByID(ctx, productUUID)
	if httperr.HandleError(w, err, http.StatusInternalServerError) {
		s.logger.For(ctx).Error("request failed", zap.Error(err))
		return
	}

	responseErr := httputils.EncodeHTTPGenericResponse(ctx, w, response)
	if httperr.HandleError(w, responseErr, http.StatusInternalServerError) {
		s.logger.For(ctx).Error("cannot marshal response", zap.Error(err))
	}
}

func (s *Server) getTraceByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	traceID := mux.Vars(r)["traceID"]
	traceUUID, err := uuid.FromString(traceID)
	//TODO improve error handling
	if httperr.HandleError(w, err, http.StatusBadRequest) {
		s.logger.For(ctx).Error("request failed, could not parse traceID", zap.Error(err))
		return
	}

	response, err := s.products.GetProductByTraceId(ctx, traceUUID)
	if httperr.HandleError(w, err, http.StatusInternalServerError) {
		s.logger.For(ctx).Error("request failed", zap.Error(err))
		return
	}

	responseErr := httputils.EncodeHTTPGenericResponse(ctx, w, response)
	if httperr.HandleError(w, responseErr, http.StatusInternalServerError) {
		s.logger.For(ctx).Error("cannot marshal response", zap.Error(err))
	}
}

func (s *Server) getMyInfo(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	u := context.Get(r, UserKey).(*openid.User)
	err := httputils.EncodeHTTPGenericResponse(ctx, w, u)
	if httperr.HandleError(w, err, http.StatusInternalServerError) {
		s.logger.For(ctx).Error("cannot marshal response", zap.Error(err))
	}
}
