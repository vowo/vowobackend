package gateway

import (
	"net/http"

	"github.com/emanoelxavier/openid2go/openid"
	"github.com/gorilla/context"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"go.uber.org/zap"
)

const UserKey = 0

var oidConfiguration *openid.Configuration

type authMiddleware struct {
	logger log.Factory
}

type userHandlerAdapter struct {
	h http.Handler
}

func (uh userHandlerAdapter) ServeHTTPWithUser(u *openid.User, rw http.ResponseWriter, req *http.Request) {
	context.Set(req, UserKey, u)
	uh.h.ServeHTTP(rw, req)
}

// AuthMiddleware authenticates the validity of a Bearer token for each request
func AuthMiddleware(providers []openid.Provider, logger log.Factory) func(next http.Handler) http.Handler {
	getProviders := func() ([]openid.Provider, error) { return providers, nil }
	a := authMiddleware{logger}
	oidConfiguration, _ := openid.NewConfiguration(openid.ProvidersGetter(getProviders), openid.ErrorHandler(a.loggingErrorHandler))

	return func(next http.Handler) http.Handler {
		handlerAdapter := userHandlerAdapter{h: next}
		return openid.AuthenticateUser(oidConfiguration, handlerAdapter)
	}
}

// getProviders returns the identity providers that will authenticate the users of the underlying service.
// A Provider is composed by its unique issuer and the collection of client IDs registered with the provider that
// are allowed to call this service.
func getProviders() ([]openid.Provider, error) {
	provider, err := openid.NewProvider("https://accounts.google.com", []string{"485821073907-is1243piagsgfftbnm05m9i37uopv2u1.apps.googleusercontent.com"})

	if err != nil {
		return nil, err
	}

	return []openid.Provider{provider}, nil
}

func (a *authMiddleware) loggingErrorHandler(e error, w http.ResponseWriter, r *http.Request) bool {
	ctx := r.Context()
	if verr, ok := e.(*openid.ValidationError); ok {
		a.logger.For(ctx).Info("verification error", zap.Any("error", verr))
		http.Error(w, verr.Message, verr.HTTPStatus)
	} else {
		a.logger.For(ctx).Error("auth error", zap.Error(e))
		w.WriteHeader(http.StatusInternalServerError)
	}

	return true
}
