package gateway

import (
	"context"
	"fmt"
	"net"
	"os"
	"strconv"

	opentracing "github.com/opentracing/opentracing-go"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-manufacturers/services/manufacturers"
	"gitlab.com/vowo/vowo-products/services/products"
	"gitlab.com/vowo/vowo-traces/services/traces"
	"go.uber.org/zap"
)

type productsHandler struct {
	products      products.Service
	traces        traces.Service
	manufacturers manufacturers.Service
	logger        log.Factory
}

func newProductsHandler(tracer opentracing.Tracer, logger log.Factory) *productsHandler {
	url := os.Getenv("PRODUCTS_URL")
	//serviceURL := resolveURL(logger, url) TODO use dns resolving
	productsClient, err := products.NewHTTPClient(
		url,
		tracer,
		logger.With(zap.String("component", "products_client")),
	)
	if err != nil {
		logger.Bg().Fatal("could not create products client", zap.Error(err))
	}

	tracesURL := os.Getenv("TRACES_URL")
	tracesClient, err := traces.NewHTTPClient(
		tracesURL,
		tracer,
		logger.With(zap.String("component", "traces_client")),
	)

	if err != nil {
		logger.Bg().Fatal("could not create traces client", zap.Error(err))
	}

	manufacturersURL := os.Getenv("MANUFACTURERS_URL")
	manufacturersClient, err := manufacturers.NewHTTPClient(
		manufacturersURL,
		tracer,
		logger.With(zap.String("component", "traces_client")),
	)

	if err != nil {
		logger.Bg().Fatal("could not create traces client", zap.Error(err))
	}

	return &productsHandler{
		products:      productsClient,
		traces:        tracesClient,
		manufacturers: manufacturersClient,
		logger:        logger,
	}
}

func resolveURL(logger log.Factory, url string) string {
	cname, rec, err := net.LookupSRV("production-product", "tcp", url)
	if err != nil {
		logger.Bg().Fatal("could not resolve products service", zap.Error(err))
	}

	logger.Bg().Info("srv cname", zap.String("cname", cname))
	serviceURL := ""
	for i := range rec {
		logger.Bg().Info("srv records", zap.Any("record", rec[i]))
		DNSbackendHost := rec[i].Target
		DNSbackendPort := strconv.Itoa(int(rec[i].Port))

		serviceURL = fmt.Sprintf("http://%v:%v/", DNSbackendHost, DNSbackendPort)
	}

	logger.Bg().Info("srv url", zap.String("url", serviceURL))
	return serviceURL
}

func (s *productsHandler) GetAllProducts(ctx context.Context) ([]*products.Product, error) {
	products, err := s.products.GetAllProducts(ctx)
	if err != nil {
		return nil, err
	}

	s.logger.For(ctx).Info("Loaded all products", zap.Any("products", products))
	return products, nil
}

func (s *productsHandler) GetProductByID(ctx context.Context, id uuid.UUID) (*products.Product, error) {
	product, err := s.products.GetProductByID(ctx, id)
	if err != nil {
		return nil, err
	}

	s.logger.For(ctx).Info("Loaded all products", zap.Any("product", product))
	return product, nil
}

type TracedProduct struct {
	Product      *products.Product           `json:"product"`
	Trace        *traces.Trace               `json:"trace"`
	TraceID      uuid.UUID                   `json:"traceId"`
	Manufacturer *manufacturers.Manufacturer `json:"manufacturer"`
}

func (s *productsHandler) GetProductByTraceId(ctx context.Context, traceID uuid.UUID) (*TracedProduct, error) {
	trace, err := s.traces.GetTraceByID(ctx, traceID)
	if err != nil {
		return nil, err
	}

	product, err := s.products.GetProductByID(ctx, trace.ProductID)
	if err != nil {
		return nil, err
	}

	manufacturer, err := s.manufacturers.GetManufacturerByID(ctx, trace.ManufacturerID)
	if err != nil {
		return nil, err
	}

	tracedProduct := &TracedProduct{
		Product:      product,
		TraceID:      trace.ID,
		Trace:        trace,
		Manufacturer: manufacturer,
	}

	s.logger.For(ctx).Info("Loaded trace info", zap.Any("traceID", traceID), zap.Any("tracedProduct", tracedProduct))
	return tracedProduct, nil
}
